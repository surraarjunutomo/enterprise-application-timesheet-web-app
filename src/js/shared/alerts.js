(function($) {
  showSwal = function(type) {
    'use strict';
    if (type === 'basic') {
      swal({
        text: 'Any fool can use a computer',
        button: {
          text: "OK",
          value: true,
          visible: true,
          className: "btn btn-primary"
        }
      })

    } else if (type === 'title-and-text') {
      swal({
        title: 'Read the alert!',
        text: 'Click OK to close this alert',
        button: {
          text: "OK",
          value: true,
          visible: true,
          className: "btn btn-primary"
        }
      })

    } else if (type === 'success-message') {
      swal({
        title: 'Congratulations!',
        text: 'You entered the correct answer',
        icon: 'success',
        button: {
          text: "Continue",
          value: true,
          visible: true,
          className: "btn btn-primary"
        }
      })

    } 
    else if (type === 'success-createaccount') {
      swal({
        title:'Account Created!',
        text:' ',
        icon: 'success',
        timer: 3000,
        button: false
      }).then(
        function() {},
        // handling the promise rejection
        function(dismiss) {
          if (dismiss === 'timer') {
            console.log('I was closed by the timer')
          }
        }
      )
     } 

     else if (type === 'failed-createaccount') {
      swal({
        title:'Create Account Failed!',
        text:' ',
        icon: 'warning',
        timer: 3000,
        button: false
      }).then(
        function() {},
        // handling the promise rejection
        function(dismiss) {
          if (dismiss === 'timer') {
            console.log('I was closed by the timer')
          }
        }
      )
     }
     else if (type === 'failed-accountexist') {
      swal({
        title:'Create Account Failed!',
        text:'Account number already exist',
        icon: 'warning',
        timer: 3000,
        button: false
      }).then(
        function() {},
        // handling the promise rejection
        function(dismiss) {
          if (dismiss === 'timer') {
            console.log('I was closed by the timer')
          }
        }
      )
     }
    else if (type === 'success-login') {
      swal({
        title:'Login Success!',
        text:' ',
        icon: 'success',
        timer: 3000,
        button: false
      }).then(
        function() {},
        // handling the promise rejection
        function(dismiss) {
          if (dismiss === 'timer') {
            console.log('I was closed by the timer')
          }
        }
      )

     } 
     else if (type === 'success-add') {
      swal({
        title:'Success Add!',
        text:' ',
        icon: 'success',
        timer: 3000,
        button: false
      }).then(
        function() {},
        // handling the promise rejection
        function(dismiss) {
          if (dismiss === 'timer') {
            console.log('I was closed by the timer')
          }
        }
      )
     }

     else if (type === 'success-upload') {
      swal({
        title:'Upload Success!',
        text:' ',
        icon: 'success',
        timer: 3000,
        button: false
      }).then(
        function() {},
        // handling the promise rejection
        function(dismiss) {
          if (dismiss === 'timer') {
            console.log('I was closed by the timer')
          }
        }
      )
     }

     else if (type === 'success-select') {
      swal({
        title:'Select Success!',
        text:' ',
        icon: 'success',
        timer: 3000,
        button: false
      }).then(
        function() {},
        // handling the promise rejection
        function(dismiss) {
          if (dismiss === 'timer') {
            console.log('I was closed by the timer')
          }
        }
      )

     }

     else if (type === 'success-add-ecl') {
      swal({
        title:'Asset Added Successfully!',
        text:' ',
        icon: 'success',
        timer: 3000,
        button: false
      }).then(
        function() {},
        // handling the promise rejection
        function(dismiss) {
          if (dismiss === 'timer') {
            console.log('I was closed by the timer')
          }
        }
      )
     }

     else if (type === 'success-calculate-ecl') {
      swal({
        title:'Assets Calculated Successfully!',
        text:' ',
        icon: 'success',
        timer: 3000,
        button: false
      }).then(
        function() {},
        // handling the promise rejection
        function(dismiss) {
          if (dismiss === 'timer') {
            console.log('I was closed by the timer')
          }
        }
      )
     }

     else if (type === 'failed-add-ecl') {
      swal({
        title:'Failed to Add Asset!',
        text:' ',
        icon: 'warning',
        timer: 3000,
        button: false
      }).then(
        function() {},
        // handling the promise rejection
        function(dismiss) {
          if (dismiss === 'timer') {
            console.log('I was closed by the timer')
          }
        }
      )
     }

     else if (type === 'failed-upload') {
      swal({
        title:'Upload Failed!',
        text:' ',
        icon: 'warning',
        timer: 3000,
        button: false
      }).then(
        function() {},
        // handling the promise rejection
        function(dismiss) {
          if (dismiss === 'timer') {
            console.log('I was closed by the timer')
          }
        }
      )
     }

     else if (type === 'error-upload') {
      swal({
        title:'Upload Failed!',
        text:' ',
        icon: 'warning',
        timer: 3000,
        button: false
      }).then(
        function() {},
        // handling the promise rejection
        function(dismiss) {
          if (dismiss === 'timer') {
            console.log('I was closed by the timer')
          }
        }
      )
     }

     else if (type === 'failed-search') {
      swal({
        title:'Search Failed',
        text:'Both start period and end period must be filled or empty',
        icon: 'warning',
        timer: 3000,
        button: false
      }).then(
        function() {},
        // handling the promise rejection
        function(dismiss) {
          if (dismiss === 'timer') {
            console.log('I was closed by the timer')
          }
        }
      )

     }

     else if (type === 'failed-calculate-ecl') {
      swal({
        title:'Calculate Asset Failed!',
        text:' ',
        icon: 'warning',
        timer: 3000,
        button: false
      }).then(
        function() {},
        // handling the promise rejection
        function(dismiss) {
          if (dismiss === 'timer') {
            console.log('I was closed by the timer')
          }
        }
      )
     }

     else if (type === 'failed-graph') {
      swal({
        title:'Generate Graph Failed!',
        text:'Application Code, Currency, and Breaktype Value Must be Filled',
        icon: 'warning',
        timer: 3000,
        button: false
      }).then(
        function() {},
        // handling the promise rejection
        function(dismiss) {
          if (dismiss === 'timer') {
            console.log('I was closed by the timer')
          }
        }
      )
     }

     else if (type === 'failed-calculate-ecl-mtm') {
      swal({
        title:'MTM value cannot be empty!',
        text:' ',
        icon: 'warning',
        timer: 3000,
        button: false
      }).then(
        function() {},
        // handling the promise rejection
        function(dismiss) {
          if (dismiss === 'timer') {
            console.log('I was closed by the timer')
          }
        }
      )
     }

 
      else if (type === 'success-sendbv') {
        swal({
          title:'Send to BV Success!',
          text:' ',
          icon: 'success',
          timer: 3000,
          button: false
        }).then(
          function() {},
          // handling the promise rejection
          function(dismiss) {
            if (dismiss === 'timer') {
              console.log('I was closed by the timer')
            }
          }
        )
       } 
     
     else if (type === 'success-edit') {
      swal({
        title:'Edit Success!',
        text:' ',
        icon: 'success',
        timer: 3000,
        button: false
      }).then(
        function() {},
        // handling the promise rejection
        function(dismiss) {
          if (dismiss === 'timer') {
            console.log('I was closed by the timer')
          }
        }
      )

     } 
     else if (type === 'success-update') {
      swal({
        title:'Update Success!',
        text:' ',
        icon: 'success',
        timer: 3000,
        button: false
      }).then(
        function() {},
        // handling the promise rejection
        function(dismiss) {
          if (dismiss === 'timer') {
            console.log('I was closed by the timer')
          }
        }
      )

     } 

     else if (type === 'success-createrole') {
      swal({
        title:'New Role Created Succesfully',
        text:' ',
        icon: 'success',
        timer: 3000,
        button: false
      }).then(
        function() {},
        // handling the promise rejection
        function(dismiss) {
          if (dismiss === 'timer') {
            console.log('I was closed by the timer')
          }
        }
      )
     }

     else if (type === 'success-delete') {
      swal({
        title:'Delete Success!',
        text:' ',
        icon: 'success',
        timer: 3000,
        button: false
      }).then(
        function() {},
        // handling the promise rejection
        function(dismiss) {
          if (dismiss === 'timer') {
            console.log('I was closed by the timer')
          }
        }
      )

     }

     else if (type === 'success-logout') {
      swal({
        title:'Log out Success!',
        text:' ',
        icon: 'success',
        timer: 3000,
        button: false
      }).then(
        function() {},
        // handling the promise rejection
        function(dismiss) {
          if (dismiss === 'timer') {
            console.log('I was closed by the timer')
          }
        }
      )

     }

     else if (type === 'failed-delete') {
      swal({
        title:'Delete Failed!',
        text:' ',
        icon: 'warning',
        timer: 3000,
        button: false
      }).then(
        function() {},
        // handling the promise rejection
        function(dismiss) {
          if (dismiss === 'timer') {
            console.log('I was closed by the timer')
          }
        }
     )

     }

     else if (type === 'failed-createrole') {
      swal({
        title:'Creating New Role Failed',
        icon: 'warning',
        timer: 3000,
        button: false
      }).then(
        function() {},
        // handling the promise rejection
        function(dismiss) {
          if (dismiss === 'timer') {
            console.log('I was closed by the timer')
          }
        }
      )

     }

     else if (type === 'failed-update') {
      swal({
        title:'Update Failed',
        icon: 'warning',
        timer: 3000,
        button: false
      }).then(
        function() {},
        // handling the promise rejection
        function(dismiss) {
          if (dismiss === 'timer') {
            console.log('I was closed by the timer')
          }
        }
      )

     }
     else if (type === 'failed-retrieve-data') {
      swal({
        title:'Loading Data Failed!',
        text: "Please check your connection",
        button: {
          text: "OK",
          value: true,
          visible: true,
          className: "btn btn-primary"
        }
      })

    }
      else if (type === 'failed-login') {
      swal({
        title:'Login Failed!',
        text: "Please check your username and password",
        icon: 'warning',
        button: {
          text: "OK",
          value: true,
          visible: true,
          className: "btn btn-primary"
        }
      })
    }

    else if (type === 'failed-sendtobv') {
      swal({
        title:'Send to BV Failed!',
        text: "",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        // icon: 'warning',
        button: {
          text: "OK",
          value: true,
          visible: true,
          className: "btn btn-primary"
        }
      })
    }

     else if (type === 'auto-close') {
      swal({
        title: 'Auto close alert!',
        text: 'I will close in 2 seconds.',
        timer: 2000,
        button: false
      }).then(
        function() {},
        // handling the promise rejection
        function(dismiss) {
          if (dismiss === 'timer') {
            console.log('I was closed by the timer')
          }
        }
      )

    } else if (type === 'warning-message-and-cancel') {
      swal({
        title: 'Are you sure you want to sign out?',
        text: "",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3f51b5',
        cancelButtonColor: '#ff4081',
        confirmButtonText: 'Great ',
        buttons: {
          cancel: {
            text: "Cancel",
            value: null,
            visible: true,
            className: "btn btn-danger",
            closeModal: true,
          },
          confirm: {
            text: "OK",
            value: true,
            visible: true,
            className: "btn btn-primary",
            closeModal: true
          }
        }
      })

    } 
    else if (type === 'access-denied') {
      swal({
        title: 'Access Denied !',
        text: " ",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3f51b5',
        cancelButtonColor: '#ff4081',
        confirmButtonText: 'Great ',
        buttons: {
          confirm: {
            text: "OK",
            value: true,
            visible: true,
            className: "btn btn-primary",
            closeModal: true
          }
        }
      })

    } else if (type === 'custom-html') {
      swal({
        content: {
          element: "input",
          attributes: {
            placeholder: "Type your password",
            type: "password",
            class: 'form-control'
          },
        },
        button: {
          text: "OK",
          value: true,
          visible: true,
          className: "btn btn-primary"
        }
      })
    }
  }

})(jQuery);
