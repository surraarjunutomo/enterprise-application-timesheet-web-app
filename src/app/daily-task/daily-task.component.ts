import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-daily-task',
  templateUrl: './daily-task.component.html',
  styleUrls: ['./daily-task.component.css'],
  providers:[DatePipe]
})
export class DailyTaskComponent implements OnInit {

  constructor(private datePipe: DatePipe) { }

  allTaskForm = 'true';
  progressForm = 'false';
  doneForm = 'false';
  pid=['#9999999','#8888888', '#7777777', '#6666666','#5555555'];
  task=['IFRS 9','PSAK 71', 'NOBU', 'BKE','Riau'];
  jumlahTask = [];
  listPid=[];
  today:any;
  inquiryTask() {

    let date = new Date();
    let month = date.getMonth();
    let day = date.getDate();
    let hour = date.getHours();
    let minute = date.getMinutes();
    let secon = date.getSeconds();
    var sekarang = new Date(date.getFullYear(),month,day,hour,minute,secon);
    this.today= this.datePipe.transform(sekarang,'EEEE, MMMM d, y');
    console.log("today : "+this.today);
    console.log()
    for (let i = 0; i < this.task.length; i++) {
      this.jumlahTask.push(this.task[i]);
      this.listPid.push(this.pid[i]);
      
    }
     console.log(this.today);
  }

  allTask() {
   
    this.allTaskForm = 'true';
    this.progressForm='false';
    this.doneForm='false';
  }

  inprogress(){
   
    this.allTaskForm = 'false';
    this.progressForm='true';
    this.doneForm='false';
  }

  completed(){

    this.allTaskForm = 'false';
    this.progressForm='false';
    this.doneForm='true';
  }
  ngOnInit(): void {
    this.inquiryTask();
  }

}
