import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { NavbarSideComponent } from './navbar-side/navbar-side.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { DailyTaskComponent } from './daily-task/daily-task.component';
import { WeeklyTaskComponent } from './weekly-task/weekly-task.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarSideComponent,
    FooterComponent,
    HeaderComponent,
    DailyTaskComponent,
    WeeklyTaskComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
