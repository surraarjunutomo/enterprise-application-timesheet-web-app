import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(public router: Router) { }

  bodyTag: HTMLBodyElement = document.getElementsByTagName('body')[0];

  login(){
    this.router.navigateByUrl("/dailytask");
  }

  ngOnInit(): void {

    this.bodyTag.setAttribute("style","background-color:powderblue");
    console.log("test");
  }

  ngOnDestroy() {
    // remove the the body classes
    this.bodyTag.removeAttribute("style");
  }

}
