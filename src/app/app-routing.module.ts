import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { NavbarSideComponent } from './navbar-side/navbar-side.component';
import { DailyTaskComponent } from './daily-task/daily-task.component';
import { WeeklyTaskComponent } from './weekly-task/weekly-task.component';

const routes: Routes = [
  {
    path: "",
    component: LoginComponent,
    outlet: "login"
  },

  {
    path: 'dailytask',
    children: [
      { path: '', component: DailyTaskComponent },
      { path: '', component: HeaderComponent, outlet: 'header' },
      { path: '', component: NavbarSideComponent, outlet: 'navbarside' },
      { path: '', component: FooterComponent, outlet: 'footer' }
    ]
  },

  {
    path: 'weeklytask', children: [
      { path: '', component: WeeklyTaskComponent },
      { path: '', component: HeaderComponent, outlet: 'header' },
      { path: '', component: NavbarSideComponent, outlet: 'navbarside' },
      { path: '', component: FooterComponent, outlet: 'footer' }
    ]
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}


