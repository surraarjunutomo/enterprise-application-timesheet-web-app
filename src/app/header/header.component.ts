import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { info } from 'console';
import { memoryUsage } from 'process';
import { Button } from 'protractor';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

}
